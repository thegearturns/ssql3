package ssql3

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"strings"

	_ "github.com/mattn/go-sqlite3"
)

// SQLResultCallBack is used in executing a DB.Exec statement
// and handling the sql.Result of the execution.
type SQLResultCallBack func(result sql.Result) error

// SQLRowsCallBack is used in executing a DB.Query statement
// and handling the *sql.Rows of the query.
type SQLRowsCallBack func(rows *sql.Rows) error

type DB struct {
	db   *sql.DB
	open bool
	name string
}

// New makes a new *DB with an open connection to the given database
// name. The user must run DB.Close when finished using the DB.
func New(name string) (*DB, error) {
	n := strings.TrimSpace(name)
	if n == "" {
		n = ":memory:"
	}
	db := &DB{
		name: n,
	}
	return db, db.Open()
}

// Open opens the database with the name that was provided
// within the DB.name.
func (d *DB) Open() error {
	if strings.TrimSpace(d.name) == "" {
		d.name = ":memory:"
	}
	db, err := sql.Open("sqlite3", d.name)
	if err != nil {
		return err
	}

	d.db = db
	d.open = true

	log.Printf("opened database[%s]", d.name)
	return nil
}

// Close ends the connection to the database and sets the DB.db = nil
// and the DB.open = false. Any errors that happen are pushed forward.
func (d *DB) Close() error {
	if d.db != nil && d.open {
		err := d.db.Close()
		if err != nil {
			return err
		}

		d.db = nil
		d.open = false

		log.Printf("closed database[%s]", d.name)
		return nil
	}
	return fmt.Errorf("database[%s] is not open", d.name)
}

// Query runs a query and handles the context and closing of prep and rows
// any errors that take place are passed on.
func (d *DB) Query(query string, rowsCallBack SQLRowsCallBack, args ...interface{}) error {
	if d.db != nil && d.open {
		ctx := context.Background()

		stmt, err := d.db.PrepareContext(ctx, query)
		if err != nil {
			return err
		}
		defer stmt.Close()

		rows, err := stmt.QueryContext(ctx, args...)
		if err != nil {
			return err
		}
		defer rows.Close()

		err = rowsCallBack(rows)
		return err
	}
	return fmt.Errorf("database[%s] is not open", d.name)
}

// Exec runs a statement and handles the context and closing of prep
// and any errors that take place are passed on.
func (d *DB) Exec(statement string, resultCallBack SQLResultCallBack, args ...interface{}) error {
	if d.db != nil && d.open {
		ctx := context.Background()

		stmt, err := d.db.PrepareContext(ctx, statement)
		if err != nil {
			return err
		}
		defer stmt.Close()

		result, err := stmt.ExecContext(ctx, args...)
		if err != nil {
			return err
		}

		err = resultCallBack(result)
		return err
	}
	return fmt.Errorf("database[%s] is not open", d.name)
}
