package ssql3

import (
	"database/sql"
	"log"
	"testing"
)

func TestBaseDBStruct(t *testing.T) {
	db := &DB{
		name: "FirstTest.db",
	}

	err := db.Open()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	createTable := `
		CREATE TABLE IF NOT EXISTS tester(id INTEGER PRIMARY KEY,test TEXT NOT NULL UNIQUE)
	`
	err = db.Exec(createTable, func(result sql.Result) error {
		rowsAffected, err := result.RowsAffected()
		if err != nil {
			return err
		}

		log.Printf("rows affected: %d", rowsAffected)
		return nil
	})

	if err != nil {
		panic(err)
	}

	db2 := &DB{}

	err = db2.Open()
	if err != nil {
		panic(err)
	}
	defer db2.Close()

	err = db2.Exec(createTable, func(result sql.Result) error {
		rowsAffected, err := result.RowsAffected()
		if err != nil {
			return err
		}

		log.Printf("rows affected: %d", rowsAffected)
		return nil
	})
	if err != nil {
		panic(err)
	}
}

func TestNewDB(t *testing.T) {
	db, err := New("SecondTest.db")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	createPeople := `
		CREATE TABLE IF NOT EXISTS people(id INTEGER PRIMARY KEY,name TEXT NOT NULL,bday INT NOT NULL)
	`

	addPeople := `
		INSERT INTO people(name,bday) VALUES(?,?)
	`
	type person struct {
		Name string
		BDay int
	}

	people := []person{
		{
			"JD",
			34,
		},
		{
			"Zen",
			36,
		},
		{
			"K",
			8,
		},
		{
			"A",
			10,
		},
	}

	resCB := func(result sql.Result) error {
		ra, err := result.RowsAffected()
		if err != nil {
			return err
		}

		log.Printf("rows affected: %d", ra)
		return nil
	}

	err = db.Exec(createPeople, resCB)
	if err != nil {
		panic(err)
	}

	for _, peep := range people {
		err = db.Exec(addPeople, resCB, peep.Name, peep.BDay)
		if err != nil {
			panic(err)
		}
	}

}
