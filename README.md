# ssql3

A wrapper for github.com/mattn/go-sqlite3 utilizing callback functions to handle sql.Result & *sql.Rows. This implementation makes it so that the enduser never has to worry about closing rows or prepared statements... only close the database itself.